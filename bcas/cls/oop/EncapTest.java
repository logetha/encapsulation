package bcas.cls.oop;

public class EncapTest {
	private String name;
	private int idNum;
	private int age;

	public int getAge() {
		return age;

	}

	public String getName() {
		return name;
	}

	public int getidNum() {
		return idNum;

	}

	public void setAge(int newAge) {
		age = newAge;
	}

	public void setName(String newName) {
		name = newName;
	}

	public void setidNum(int newidNum) {
		idNum = newidNum;
	}
}
